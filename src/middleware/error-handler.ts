import * as Boom from 'boom';
import boom from 'boom';
import logger from '../logger/logger';

export function notFound(req: any, res: any, next: any) {
	return next(boom.notFound('Endpoint does not exist'));
}

export function handleError(err: Boom, req: any, res: any, next: any) {
	if (err.isServer) logger.error(err);
	if (err.output) return res.status(err.output.statusCode).json(err.output.payload);
	// handle all other errors generically with code 500
	logger.error(err);
	const serverError = boom.badImplementation();
	return res.status(serverError.output.statusCode).json(serverError.output.payload);
}
