import winston from 'winston';

class Logger {
	logger: any;
	constructor() {
		this.logger = winston.createLogger({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.json(),
				winston.format.simple()
			),
			transports: [ new winston.transports.Console() ]
		});
		this.logger.stream = {
			write: (message: string, encoding: string) => {
				this.logger.info(message);
			}
		};
	}

	info(...args: any) {
		this.logger.info(...args);
	}

	warn(...args: any) {
		this.logger.warn(...args);
	}

	error(...args: any) {
		this.logger.error(...args);
	}

	getStream() {
		return this.logger.stream;
	}
}

export default new Logger();
