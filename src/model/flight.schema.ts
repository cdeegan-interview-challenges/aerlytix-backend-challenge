import mongoose from 'mongoose';
// @ts-ignore
import sequence from 'mongoose-sequence';
import Aircraft from './aircraft.schema';
import { IFlight } from './interface/IFlight';

const AutoIncrement = sequence(mongoose);
const Schema = mongoose.Schema;

const flightSchema = new Schema(
	{
		flight_number: { type: String, required: true, unique: true },
		aircraft: { type: Aircraft.schema, required: true },
		departure_airport: { type: String, required: true },
		departure_timestamp: { type: Date, required: true, set: (d: number) => new Date(d * 1000) },
		arrival_airport: { type: String, required: true },
		arrival_timestamp: { type : Date, required: true, set: (d: number) => new Date(d * 1000) }
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

flightSchema.pre('validate', async function(this: any, next) {
	if (!this.aircraft) return next(new Error('Aircraft does not exist'));
	next();
});

flightSchema.statics.getFlights = function(query: any): Promise<mongoose.Document[]> {
	return this.find(query).exec();
};

flightSchema.statics.createFlight = async function(data: any): Promise<mongoose.Document> {
	const aircraftData: any = await Aircraft.find({ registration_code: data.registration }).exec();
	if (aircraftData && aircraftData.length > 0) data.aircraft = aircraftData[0];
	const flight = new this(data);
	return flight.save();
};

flightSchema.plugin(AutoIncrement, { id: 'flight_schema_counter', inc_field: 'id' });


const Flight: IFlight = mongoose.model<mongoose.Document, IFlight>('Flight', flightSchema);
export default Flight;
