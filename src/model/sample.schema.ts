import mongoose from 'mongoose';
// @ts-ignore
import sequence from 'mongoose-sequence';
// @ts-ignore
import * as $ from 'mongo-dot-notation';

const AutoIncrement = sequence(mongoose);
const Schema = mongoose.Schema;

const sampleSchema = new Schema(
	{
		name: { type: String, default: '' }
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

sampleSchema.statics.getSample = function(id: number, f: () => void) {
	return this.findOne({ id }, f);
};

sampleSchema.statics.getSamples = function(query: any, f: () => void) {
	return this.find(query, f);
};

sampleSchema.statics.deleteSample = function(id: string, f: () => void) {
	return this.findOneAndDelete({ id }, f);
};

sampleSchema.statics.deleteSamples = function(query: any, f: () => void) {
	return this.deleteMany(query, f);
};

sampleSchema.statics.createSample = function(data: any, f: () => void) {
	const sample = new this(data);
	return sample.save(f);
};

sampleSchema.statics.updateSample = function(id: number, data: any, f: () => void) {
	return this.findOneAndUpdate({ id }, $.flatten(data), { new: true, runValidators: true }, f);
};

sampleSchema.plugin(AutoIncrement, { id: 'sample_schema_counter', inc_field: 'id' });

const Sample: any = mongoose.model('Sample', sampleSchema);
export default Sample;
