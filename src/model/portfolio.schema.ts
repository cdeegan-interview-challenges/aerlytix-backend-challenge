import mongoose from 'mongoose';
import Aircraft from './aircraft.schema';
// @ts-ignore
import sequence from 'mongoose-sequence';
// @ts-ignore
import * as $ from 'mongo-dot-notation';
import { IAircraft } from './interface/IAircraft';
import { IPortfolio } from './interface/IPortfolio';

const AutoIncrement = sequence(mongoose);

const portfolioSchema = new mongoose.Schema(
	{
		name: { type: String, required: true, unique: true },
		aircraft: [Aircraft.schema]
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

portfolioSchema.pre('save', async function(this: any, next) {
	if (this.aircraft && this.aircraft.length > 0) {
		this.aircraft = await filterAircraft(this.aircraft);
	}
	next();
});

portfolioSchema.pre('findOneAndUpdate', async function(next) {
	const data = this.getUpdate();
	if (data.$set.aircraft && data.$set.aircraft.length > 0) {
		data.$set.aircraft = await filterAircraft(data.$set.aircraft);
	}
	next();
});

const filterAircraft = async function(aircraft: IAircraft[]): Promise<IAircraft[]> {
	const existingAircraft: any[] = await Aircraft.find({ registration_code: { $in: aircraft.map((a: IAircraft) => a.registration_code)}}).exec();
	return aircraft.filter((aircraft: IAircraft, index: number, self: IAircraft[]) => {
		// remove duplicates within current query
		const existingIndexWithinArray = self.findIndex(a => a.registration_code === aircraft.registration_code);
		const isDuplicateWithinQuery = index !== existingIndexWithinArray;
		if (isDuplicateWithinQuery) return false;
		// remove elements not already existing in database
		const existingIndexWithinDatabase = existingAircraft.findIndex(a => a.registration_code === aircraft.registration_code);
		const isDuplicateWithinDatabase = existingIndexWithinDatabase > -1;
		// override the aircraft to match the database value
		if (isDuplicateWithinDatabase) aircraft = existingAircraft[existingIndexWithinDatabase];
		return isDuplicateWithinDatabase;
	});
}

portfolioSchema.statics.getPortfolio = function(id: number): Promise<mongoose.Document> {
	return this.findOne({ id }).exec();
};

portfolioSchema.statics.getPortfolios = function(query: any): Promise<mongoose.Document[]> {
	return this.find(query).exec();
};

portfolioSchema.statics.deletePortfolio = function(id: string): Promise<mongoose.Document> {
	return this.findOneAndDelete({ id }).exec();
};

portfolioSchema.statics.deletePortfolioAircraft = async function(id: number, registration_code: string): Promise<any> {
	const portfolio: IPortfolio = await this.findOne({ id }).exec();
	if (!portfolio) return { portfolioFound: false, aircraftFound: false, data: {}};
	const initialAircraftCount = portfolio.aircraft.length;
	portfolio.aircraft = portfolio.aircraft.filter(a => a.registration_code !== registration_code);
	if (portfolio.aircraft.length === initialAircraftCount) return { portfolioFound: true, aircraftFound: false, data: {}};
	const data = $.flatten({ aircraft: portfolio.aircraft });
	const result = await this.findOneAndUpdate({ id: portfolio.id }, data, { new: true }).exec();
	return { portfolioFound: true, aircraftFound: true, data: result };
};

portfolioSchema.statics.createPortfolio = function(data: any): Promise<mongoose.Document> {
	const portfolio = new this(data);
	return portfolio.save();
};

portfolioSchema.statics.updatePortfolio = function(id: number, data: any): Promise<mongoose.Document> {
	return this.findOneAndUpdate({ id }, $.flatten(data), { new: true, runValidators: true }).exec();
};

portfolioSchema.plugin(AutoIncrement, { id: 'portfolio_schema_counter', inc_field: 'id' });

const Portfolio: IPortfolio = mongoose.model<mongoose.Document, IPortfolio>('Portfolio', portfolioSchema);
export default Portfolio;
