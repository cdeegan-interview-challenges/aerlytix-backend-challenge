import mongoose from 'mongoose';

export interface IAircraft extends mongoose.Model<mongoose.Document> {
	id: number;
	registration_code: string;
	aircraft_type: string;
	createAircraft(data: any): Promise<mongoose.Document>;
}
