import mongoose from 'mongoose';
import { IAircraft } from './IAircraft';


export interface IFlight extends mongoose.Model<mongoose.Document> {
	id: number;
	flight_number: string;
	aircraft: IAircraft;
	departure_airport: string;
	departure_timestamp: string;
	arrival_airport: number;
	arrival_timestamp: number;
	getFlights(data: any): Promise<mongoose.Document[]>;
	createFlight(data: any): Promise<mongoose.Document>;
}
