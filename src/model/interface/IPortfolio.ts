import mongoose from 'mongoose';
import { IAircraft } from './IAircraft';

export interface IPortfolio extends mongoose.Model<mongoose.Document> {
	id: number;
	name: string;
	aircraft: IAircraft[];
	getPortfolio(id: number): Promise<mongoose.Document>;
	getPortfolios(query: any): Promise<mongoose.Document[]>;
	deletePortfolio(id: number): Promise<mongoose.Document>;
	deletePortfolioAircraft(id: number, registration_code: string): Promise<any>;
	createPortfolio(data: any): Promise<mongoose.Document>;
	updatePortfolio(id: number, data: any): Promise<mongoose.Document>;
}
