import mongoose from 'mongoose';
// @ts-ignore
import sequence from 'mongoose-sequence';
import { IAircraft } from './interface/IAircraft';

const AutoIncrement = sequence(mongoose);
const Schema = mongoose.Schema;

const aircraftSchema = new Schema(
	{
		registration_code: { type: String, required: true },
		aircraft_type: { type: String, required: true }
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

aircraftSchema.statics.createAircraft = function(data: any): Promise<mongoose.Document> {
	const aircraft = new this(data);
	return aircraft.save();
};

aircraftSchema.plugin(AutoIncrement, { id: 'aircraft_schema_counter', inc_field: 'id' });

const Aircraft: IAircraft = mongoose.model<mongoose.Document, IAircraft>('Aircraft', aircraftSchema);
export default Aircraft;
