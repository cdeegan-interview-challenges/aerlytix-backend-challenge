import boom from 'boom';
// @ts-ignore
import Sample from '../model/sample.schema';

export function getSample(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	Sample.getSample(req.params.id, (err: any, sample: any) => {
		if (err) return next(boom.badImplementation(err));
		if (!sample) return next(boom.notFound(`Sample ${req.params.id} not found`));
		res.json(sample);
	});
}

export function getSamples(req: any, res: any, next: any) {
	Sample.getSamples(req.query, (err: any, samples: any) => {
		if (err) return next(boom.badImplementation(err));
		else res.json(samples);
	});
}

export function updateSample(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	Sample.updateSample(req.params.id, req.body, (err: any, sample: any) => {
		if (err) return next(boom.badImplementation(err));
		else res.json(sample);
	});
}

export function deleteSample(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	Sample.deleteSample(req.params.id, (err: any, sample: any) => {
		if (err) return next(boom.badImplementation(err));
		else res.json(sample);
	});
}

export function deleteSamples(req: any, res: any, next: any) {
	Sample.deleteSamples(req.query, (err: any, samples: any) => {
		if (err) return next(boom.badImplementation(err));
		else res.json(samples);
	});
}

export function createSample(req: any, res: any, next: any) {
	Sample.createSample(req.body, (err: any, sample: any) => {
		if (err) return next(boom.badImplementation(err));
		else res.json(sample);
	});
}
