import boom from 'boom';
// // @ts-ignore
import Portfolio from '../model/portfolio.schema';

export async function getPortfolio(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	try {
		const portfolio = await Portfolio.getPortfolio(req.params.id);
		if (!portfolio) return next(boom.notFound(`Portfolio ${req.params.id} not found`));
		return res.json(portfolio);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}

export async function deletePortfolioAircraft(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	if (!req.query.registration_code) return next(boom.badData('Missing registration_code query parameter'));
	const result = await Portfolio.deletePortfolioAircraft(req.params.id, req.query.registration_code);
	if (!result.portfolioFound) return next(boom.notFound(`Portfolio ${req.params.id} not found`));
	if (!result.aircraftFound) return next(boom.notFound(`Portfolio ${req.params.id} has no aircraft with registration ${req.query.registration_code}`));
	return res.json(result.data);
}

export async function getPortfolios(req: any, res: any, next: any) {
	try {
		const portfolios = await Portfolio.getPortfolios(req.query);
		return res.json(portfolios);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}

export async function updatePortfolio(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	try {
		const portfolio = await Portfolio.updatePortfolio(req.params.id, req.body);
		return res.json(portfolio);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}

export async function deletePortfolio(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	try {
		const portfolio = await Portfolio.deletePortfolio(req.params.id);
		return res.json(portfolio);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}

export async function createPortfolio(req: any, res: any, next: any) {
	try {
		const portfolio = await Portfolio.createPortfolio(req.body);
		return res.json(portfolio);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}
