import boom from 'boom';
import Flight from '../model/flight.schema';
import { IAircraft } from '../model/interface/IAircraft';
import Portfolio from '../model/portfolio.schema';
import StatisticsService from '../services/statistics.service';

export async function getPortfolioStatistics(req: any, res: any, next: any) {
	if (!req.params.id) return next(boom.badData('Missing id'));
	try {
		const limit = `${req.query.limit}`.toLowerCase() === 'true';
		const portfolio: any = await Portfolio.getPortfolio(req.params.id);
		if (!portfolio) return next(boom.notFound(`Portfolio ${req.params.id} not found`));
		const flightAircraftIds = portfolio.aircraft.map((a: IAircraft) => a.registration_code);
		const flights = await Flight.getFlights({ 'aircraft.registration_code': { $in: flightAircraftIds } });
		return res.json(StatisticsService.sumFlightHours(flights as any, ['registration_code', 'aircraft_type'], 'registration_code', limit));
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}

export async function getGlobalStatistics(req: any, res: any, next: any) {
	try {
		const limit = `${req.query.limit}`.toLowerCase() === 'true';
		const flights = await Flight.getFlights({});
		return res.json(StatisticsService.sumFlightHours(flights as any, ['aircraft_type'], 'aircraft_type', limit));
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}
