import boom from 'boom';
import Flight from '../model/flight.schema';
import FlightValidatorService from '../services/flight-validator.service';

export async function createFlight(req: any, res: any, next: any) {
	const flightValidity = FlightValidatorService.validate(req.body);
	if (flightValidity.error) return next(flightValidity.boomErr);
	try {
		const flight = await Flight.createFlight(flightValidity.data);
		return res.json(flight);
	} catch (e) {
		return next(boom.badImplementation(e));
	}
}
