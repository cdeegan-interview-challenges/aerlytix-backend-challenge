import express from 'express';
import * as flightController from '../controllers/flight-controller';
import * as portfolioController from '../controllers/portfolio-controller';
import * as statisticsController from '../controllers/statistics.controller';

const router = express.Router();

router.route('/portfolio/:id/aircraft/')
	.delete(portfolioController.deletePortfolioAircraft);

router.route('/portfolio/:id/statistics')
	.get(statisticsController.getPortfolioStatistics);

router.route('/portfolio/:id')
	.get(portfolioController.getPortfolio)
	.put(portfolioController.updatePortfolio)
	.delete(portfolioController.deletePortfolio);

router.route('/portfolio/')
	.get(portfolioController.getPortfolios)
	.post(portfolioController.createPortfolio);

router.route('/flight/')
	.post(flightController.createFlight);

router.route('/statistics/')
	.get(statisticsController.getGlobalStatistics);

export default router;
