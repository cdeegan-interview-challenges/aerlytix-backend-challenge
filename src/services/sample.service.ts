class SampleService {
	/**
	 * Squares the given number
	 */
	squareNumber(number: number): number {
		return Math.pow(number, 2);
	}
}

export default new SampleService();
