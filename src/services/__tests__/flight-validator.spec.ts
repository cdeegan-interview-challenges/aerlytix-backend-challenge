import FlightValidatorService from '../flight-validator.service';

describe('FlightValidatorService', () => {
	it('should validate request properties', () => {
		expect(FlightValidatorService.validateFlightPropertiesExist({})).toBeDefined();
		expect(FlightValidatorService.validateFlightPropertiesExist({
			flight_number: '123',
			registration: 'ABC-123',
			departure_airport: 'XYZ',
			departure_timestamp: 1602073227,
			arrival_airport: 'ABC',
			arrival_timestamp: 1602073227,
		})).toBeUndefined();
	});

	it('should validate flight timestamps', () => {
		expect(FlightValidatorService.validateFlightTimestamps({
			departure_timestamp: 1602073001,
			arrival_timestamp: 1602073000,
		})).toBeDefined();
		expect(FlightValidatorService.validateFlightTimestamps({
			departure_timestamp: 1602073000,
			arrival_timestamp: 1902073000,
		})).toBeDefined();
		expect(FlightValidatorService.validateFlightTimestamps({
			departure_timestamp: 1602073000,
			arrival_timestamp: 1602073000,
		})).toBeUndefined();
		expect(FlightValidatorService.validateFlightTimestamps({
			departure_timestamp: 1602073000,
			arrival_timestamp: 1602077000,
		})).toBeUndefined();
	});

	it('should validate flight airports', () => {
		expect(FlightValidatorService.validateFlightAirports({
			arrival_airport: 'ABC',
			departure_airport: 'ABC',
		})).toBeDefined();
		expect(FlightValidatorService.validateFlightAirports({
			arrival_airport: 'CDG',
			departure_airport: 'ABC',
		})).toBeDefined();
		expect(FlightValidatorService.validateFlightAirports({
			arrival_airport: 'ABC',
			departure_airport: 'STN',
		})).toBeDefined();
		expect(FlightValidatorService.validateFlightAirports({
			arrival_airport: 'DUB',
			departure_airport: 'MXP',
		})).toBeUndefined();
	});
});
