import * as Boom from 'boom';
import boom from 'boom';


class FlightValidatorService {

	private VALID_AIRPORTS = ['DUB', 'STN', 'MXP', 'CDG'];

	/**
	 * The main validation method that performs a number of checks on a flight, one by one.
	 */
	validate(requestData: any): { error: boolean, data?: any, boomErr?: Boom } {
		const transformedData = this.transformRequest(requestData);
		if (transformedData.boomErr) return { error: true, boomErr: transformedData.boomErr };
		requestData = transformedData.data;

		const propertiesInvalid = this.validateFlightPropertiesExist(requestData);
		if (propertiesInvalid) return { error: true, boomErr: propertiesInvalid };

		const propertyTypesInvalid = this.validateFlightPropertyTypes(requestData);
		if (propertyTypesInvalid) return { error: true, boomErr: propertyTypesInvalid };

		const timestampsInvalid = this.validateFlightTimestamps(requestData);
		if (timestampsInvalid) return { error: true, boomErr: timestampsInvalid };

		const airportsInvalid = this.validateFlightAirports(requestData);
		if (airportsInvalid) return { error: true, boomErr: airportsInvalid };

		return { error: false, data: requestData };
	}

	/*
		Parse single-element array requests, reject multi-element array requests
	*/
	transformRequest(requestData: any): { data?: any, boomErr?: Boom } {
		if (Array.isArray(requestData)) {
			if (requestData.length > 1) return { boomErr: boom.badData(`Only one flight can be processed per request`) };
			return { data: requestData[0] };
		}
		return { data: requestData };
	}

	/*
		Validate that all the properties required of a flight are present
	*/
	validateFlightPropertiesExist(requestData: any): Boom | undefined {
		const missingData = [];
		if (!requestData.flight_number) missingData.push('flight_data');
		if (!requestData.registration) missingData.push('registration');
		if (!requestData.departure_airport) missingData.push('departure_airport');
		if (!requestData.departure_timestamp) missingData.push('departure_timestamp');
		if (!requestData.arrival_airport) missingData.push('arrival_airport');
		if (!requestData.arrival_timestamp) missingData.push('arrival_timestamp');
		if (missingData.length > 0) return boom.badData(`Flight data is missing required properties: ${missingData.join(', ')}`);
		return;
	}

	/*
		Validate that all the properties of the request have correct types / forms
	*/
	validateFlightPropertyTypes(requestData: any): Boom | undefined {
		// TODO: validate timestamps are numbers + valid, others are strings
		return;
	}

	/*
		Validate that departure time is before arrival time and both are past dates
	*/
	validateFlightTimestamps(requestData: any): Boom | undefined {
		const currentUTCTime = Math.round(new Date().valueOf() / 1000);
		if (requestData.departure_timestamp > currentUTCTime) return boom.badData('Departure time must be in the past');
		if (requestData.arrival_timestamp > currentUTCTime) return boom.badData('Arrival time must be in the past');
		if (requestData.departure_timestamp > requestData.arrival_timestamp) return boom.badData('Departure time must be before arrival time');
	}

	/*
		Validate that departure and arrival airports exist and are not the same value
	*/
	validateFlightAirports(requestData: any): Boom | undefined {
		if (requestData.arrival_airport === requestData.departure_airport) return boom.badData('Departure and arrival airports must differ');
		if (this.VALID_AIRPORTS.indexOf(requestData.departure_airport) === -1) return boom.badData(`Airport ${requestData.departure_airport} does not exist`);
		if (this.VALID_AIRPORTS.indexOf(requestData.arrival_airport) === -1) return boom.badData(`Airport ${requestData.arrival_airport} does not exist`);

	}
}

export default new FlightValidatorService();
