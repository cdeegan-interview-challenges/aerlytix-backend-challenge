import { IFlight } from '../model/interface/IFlight';

class StatisticsService {
	/**
	 * gets the sum of all flight hours by the provided key property
	 * optional limit property will get statistics of last 24 hours
	 */
	sumFlightHours(flights: IFlight[], displayProperties: string[] = [],
		key: ('registration_code' | 'aircraft_type') = 'registration_code', limit?: boolean): any {
			const result: any = {};
			const yesterday = new Date();
			yesterday.setDate(yesterday.getDate() - 1);
			flights.forEach(f => {
				const flightDepartureTime = Math.round(new Date(f.departure_timestamp).valueOf() / 1000);
				const flightArrivalTime = Math.round(new Date(f.arrival_timestamp).valueOf() / 1000);
				const withinRange = limit ? flightArrivalTime > Math.round(yesterday.valueOf() / 1000) : true;
				if (withinRange) {
					const aircraftPropertyKey = f.aircraft[key];
					result[aircraftPropertyKey] = result[aircraftPropertyKey] || { flightHours: 0, flightCount: 0 };
					result[aircraftPropertyKey].registration_code = f.aircraft.registration_code;
					result[aircraftPropertyKey].aircraft_type = f.aircraft.aircraft_type;
					result[aircraftPropertyKey].flightHours += (flightArrivalTime - flightDepartureTime);
					result[aircraftPropertyKey].flightCount++;
				}
			});
			return this.formatFlightHourResponse(result, displayProperties);
	}

	/**
	 * Formats flight statistics for an API response
	 */
	formatFlightHourResponse(data: any, properties: string[]): any[] {
		const results: any[] = [];
		Object.keys(data).forEach(k => {
			const result: any = {
				flight_hours: this.formatSecondsToHours(data[k].flightHours),
				flight_count: data[k].flightCount
			};
			properties.forEach(p => {
				if (data[k].hasOwnProperty(p)) result[p] = data[k][p];
			});
			results.push(result);
		});
		return results;
	}

	/**
	 * Formats seconds to H:m:s format as per function here: https://stackoverflow.com/a/5539081
	 */
	formatSecondsToHours(seconds: number): string {
		const h = Math.floor(seconds / 3600);
		const m = Math.floor(seconds % 3600 / 60);
		const s = Math.floor(seconds % 3600 % 60);

		return `${('0' + h).slice(-2)}:${('0' + m).slice(-2)}:${('0' + s).slice(-2)}`;
	}
}

export default new StatisticsService();
