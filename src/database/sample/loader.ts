import logger from '../../logger/logger';
import Aircraft from '../../model/aircraft.schema';
import Flight from '../../model/flight.schema';
import Portfolio from '../../model/portfolio.schema';
import { aircraft, flights, portfolios } from './data';

export async function loadSampleData() {
	try {
		const createSampleAircraft = aircraft.map(a => Aircraft.createAircraft(a));
		await createSampleAircraft.reduce(async (acc: any, cur) => {
			await acc;
			return cur;
		}, Promise.resolve());
		const createSamplePortfolios = portfolios.map(p => Portfolio.createPortfolio(p));
		await createSamplePortfolios.reduce(async (acc: any, cur) => {
			await acc;
			return cur;
		}, Promise.resolve());
		const createSampleFlights = flights.map(f => Flight.createFlight(f));
		await createSampleFlights.reduce(async (acc: any, cur) => {
			await acc;
			return cur;
		}, Promise.resolve());
	} catch (e) {
		logger.warn(`Error creating sample data: ${e}`);
	}
}
