export const aircraft = [
	{ registration_code: 'ZS-GAO', aircraft_type: 'A320-200' },
	{ registration_code: 'B-6636', aircraft_type: 'A320-200' },
	{ registration_code: 'LY-BFM', aircraft_type: 'B737-800' },
	{ registration_code: 'G-GDFW', aircraft_type: 'B737-800' },
	{ registration_code: 'XA-AMZ', aircraft_type: 'B737-800' }
];

export const portfolios = [
	{ name: 'portfolio_1' },
	{ name: 'portfolio_2' },
	{
		name: 'portfolio_3',
		aircraft: [
			{ registration_code: 'ZS-GAO', aircraft_type: 'A320-200' },
			{ registration_code: 'B-6636', aircraft_type: 'A320-200' }
		]
	}
];

export const flights = [
	{
		flight_number: 'FR1454',
		registration: 'ZS-GAO',
		departure_airport: 'DUB',
		departure_timestamp: 1595931449,
		arrival_airport: 'MXP',
		arrival_timestamp: 1595939700
	},
	{
		flight_number: 'DB1851',
		registration: 'B-6636',
		departure_airport: 'DUB',
		departure_timestamp: 1595931349,
		arrival_airport: 'CDG',
		arrival_timestamp: 1595939700
	},
	{
		flight_number: 'DB1850',
		registration: 'B-6636',
		departure_airport: 'CDG',
		departure_timestamp: 1602073227,
		arrival_airport: 'DUB',
		arrival_timestamp: 1602078370
	},
	{
		flight_number: 'FR1455',
		registration: 'G-GDFW',
		departure_airport: 'CDG',
		departure_timestamp: 1602073207,
		arrival_airport: 'DUB',
		arrival_timestamp: 1602078300
	}
];
