# Aerlytix Backend Interview Test




# Instructions

## Installing dependencies

`npm install`

## Running the project

`npm run dev`

## Running tests

`npm run test`

# Usage
- `GET /api/portfolio` gets a list of all portfolios
- `POST /api/portfolio` creates a new portfolio
- `GET /api/portfolio/:id` gets the specified portfolio
- `PUT /api/portfolio/:id` updates the specified portfolio
- `DELETE /api/portfolio/:id/aircraft?registration_code=123` removes the aircraft with the specified reg. no. from the specified portfolio
- `DELETE /api/portfolio/:id` deletes the specified portfolio
- `POST /api/flight` creates a new flight
	- `{
"flight_number": "FR1454",
"registration": "ZS-GAO",
"departure_airport": "DUB",
"departure_timestamp": 1595931449,
"arrival_airport": "MXP",
"arrival_timestamp": 1595939700,
}`
- `GET /api/statistics` gets global flight statistics (`?limit=true` to limit to past 24 hours only)
- `GET /api/portfolio/:id/statistics` gets a the specified portfolio's flight statistics (`?limit=true` to limit to past 24 hours only)

# Notes

## Database implementation
This project uses an [in-memory mongodb server](mongodb-memory-server) to store data. On first run, this library will install the necessary executables to start a mongodb server within the node process.

## Improvements
This project would be suited to a library such as [JOI](https://www.npmjs.com/package/joi) since it allows for schema-based validation.

Additionally, there are some error cases that could be handled better to provide more descriptive feedback to the API agent.

There are some data issues caused by the use of nested schemas, since those are not necessarily new documents, rather they should be references to existing documents -- for example a portfolio should reference a set of aircraft, rather than contain the aircraft data itself, since this is treated as a new document.

Due to time constraints, there are some other extensions that could be made, as well as additional tests
